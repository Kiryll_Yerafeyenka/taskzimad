package by.svt.kirill.testtaskzimad.presentation.feature.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.presentation.feature.main.adapter.viewholder.AnimalItemHolder;

public class AnimalItemsListAdapter extends RecyclerView.Adapter<AnimalItemHolder> {
    private ArrayList<AnimalsParams> animalsParamsArrayList;

    public AnimalItemsListAdapter(ArrayList<AnimalsParams> animalsParamsArrayList) {
        this.animalsParamsArrayList = animalsParamsArrayList;
    }

    @NonNull
    @Override
    public AnimalItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent,false);
        return new AnimalItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalItemHolder holder, int position) {
        holder.populate(animalsParamsArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return animalsParamsArrayList.size();
    }
}

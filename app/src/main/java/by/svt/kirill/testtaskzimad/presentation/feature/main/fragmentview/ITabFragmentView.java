package by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview;

import java.util.List;

import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;

public interface ITabFragmentView {
    void populate(List<AnimalsParams> animalsParamsList);
}

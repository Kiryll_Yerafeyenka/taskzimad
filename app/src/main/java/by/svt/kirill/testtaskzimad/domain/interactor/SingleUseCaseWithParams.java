package by.svt.kirill.testtaskzimad.domain.interactor;

import io.reactivex.Single;

public interface SingleUseCaseWithParams<Params, T> {
    Single<T> execute(Params params);
}

package by.svt.kirill.testtaskzimad.presentation.feature.main.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.domain.dto.base.Resource;
import by.svt.kirill.testtaskzimad.domain.dto.base.ServerResponse;
import by.svt.kirill.testtaskzimad.domain.interactor.mainlist.MainListUseCase;
import by.svt.kirill.testtaskzimad.presentation.base.viewmodel.BaseViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseViewModel {
    private MainListUseCase mainListUseCase = new MainListUseCase();

    private MutableLiveData<Resource<ServerResponse<List<AnimalsParams>>>> _animalsParamsMutableLiveData = new MutableLiveData<>();

    public LiveData<Resource<ServerResponse<List<AnimalsParams>>>> getAnimalsParamsLiveData() {
        return _animalsParamsMutableLiveData;
    }

    public void getAnimalsList(String params) {
        addDisposable(mainListUseCase.execute(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> _animalsParamsMutableLiveData.setValue(Resource.loading()))
                .subscribe(response -> _animalsParamsMutableLiveData.setValue(Resource.success(response)),
                        error -> _animalsParamsMutableLiveData.setValue(Resource.error(error))));

    }
}

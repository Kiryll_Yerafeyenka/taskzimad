package by.svt.kirill.testtaskzimad.utils;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import by.svt.kirill.testtaskzimad.R;

public class PicassoUtils {

    public static void loadImage(String url, ImageView targetView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_launcher_foreground)
                .fit()
                .into(targetView);
    }

    public static void loadDetailsImage(String url, ImageView targetView) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.ic_launcher_foreground)
                .fit()
                .centerInside()
                .into(targetView);
    }
}
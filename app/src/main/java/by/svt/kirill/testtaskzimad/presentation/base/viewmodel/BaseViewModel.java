package by.svt.kirill.testtaskzimad.presentation.base.viewmodel;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel {

    private final CompositeDisposable disposables = new CompositeDisposable();

    protected void addDisposable(Disposable subscribe) {
        disposables.add(subscribe);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }
}

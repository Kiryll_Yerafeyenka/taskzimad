package by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.utils.PicassoUtils;

public class DetailsItemFragmentView extends LinearLayout implements IDetailsItemFragmentView {
    private ImageView detailsAnimalImage;
    private TextView detailsAnimalNumber;
    private TextView detailsAnimalTitle;

    public DetailsItemFragmentView(Context context) {
        super(context);
    }

    public DetailsItemFragmentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DetailsItemFragmentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
    }

    private void findViews() {
        detailsAnimalImage = findViewById(R.id.details_image);
        detailsAnimalNumber = findViewById(R.id.details_number_text);
        detailsAnimalTitle = findViewById(R.id.details_main_text);
    }

    @Override
    public void populate(AnimalsParams model) {
        PicassoUtils.loadDetailsImage(model.getUrl(), detailsAnimalImage);
        detailsAnimalNumber.setText(model.getNumber());
        detailsAnimalTitle.setText(model.getTitle());
    }
}

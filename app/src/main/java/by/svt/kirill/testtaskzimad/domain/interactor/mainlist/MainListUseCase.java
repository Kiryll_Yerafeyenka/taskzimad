package by.svt.kirill.testtaskzimad.domain.interactor.mainlist;

import java.util.List;

import by.svt.kirill.testtaskzimad.data.repository.MainListRepository;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.domain.dto.base.ServerResponse;
import by.svt.kirill.testtaskzimad.domain.interactor.SingleUseCaseWithParams;
import by.svt.kirill.testtaskzimad.domain.repository.IMainListRepository;
import io.reactivex.Single;

public class MainListUseCase implements SingleUseCaseWithParams<String, ServerResponse<List<AnimalsParams>>> {
    private IMainListRepository listRepository = new MainListRepository();

    @Override
    public Single<ServerResponse<List<AnimalsParams>>> execute(String s) {
        return listRepository.getAnimalsList(s);
    }
}

package by.svt.kirill.testtaskzimad.presentation.feature.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.presentation.feature.main.activity.DetailItemActivity;
import by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview.IDetailsItemFragmentView;

public class DetailsFragment extends Fragment {
    private AnimalsParams animalsParams;
    private IDetailsItemFragmentView fragmentView;

    public static DetailsFragment newInstance(AnimalsParams model) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DetailItemActivity.EXTRA_ANIMAL_MODEL, model);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            animalsParams = (AnimalsParams) getArguments().getSerializable(DetailItemActivity.EXTRA_ANIMAL_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tab_item_details_fragment_view, container, false);
        fragmentView = (IDetailsItemFragmentView) root;
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentView.populate(animalsParams);
    }
}

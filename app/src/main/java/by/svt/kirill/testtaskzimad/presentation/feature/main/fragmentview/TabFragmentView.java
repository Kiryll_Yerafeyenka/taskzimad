package by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.presentation.feature.main.adapter.AnimalItemsListAdapter;


public class TabFragmentView extends FrameLayout implements ITabFragmentView {
    private RecyclerView recyclerView;

    public TabFragmentView(@NonNull Context context) {
        super(context);
    }

    public TabFragmentView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TabFragmentView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        recyclerView = findViewById(R.id.tab_recycler_view);
    }

    @Override
    public void populate(List<AnimalsParams> animalsParamsList) {
        recyclerView.setAdapter(new AnimalItemsListAdapter(new ArrayList<>(animalsParamsList)));
    }
}

package by.svt.kirill.testtaskzimad.domain.dto.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import static by.svt.kirill.testtaskzimad.domain.dto.base.Status.ERROR;
import static by.svt.kirill.testtaskzimad.domain.dto.base.Status.LOADING;
import static by.svt.kirill.testtaskzimad.domain.dto.base.Status.SUCCESS;

public class Resource<T> {
    @NonNull
    private final Status status;

    @Nullable
    private final Throwable throwable;

    @Nullable
    private final T data;

    public Resource(@NonNull Status status, @Nullable T data, @Nullable Throwable throwable) {
        this.status = status;
        this.data = data;
        this.throwable = throwable;
    }

    public static <T> Resource<T> success(@Nullable T data) {
        return new Resource<>(SUCCESS, data, null);
    }

    public static <T> Resource<T> error(Throwable throwable) {
        return new Resource<>(ERROR, null, throwable);
    }

    public static <T> Resource<T> loading() {
        return new Resource<>(LOADING, null, null);
    }

    @NonNull
    public Status getStatus() {
        return status;
    }

    @Nullable
    public Throwable getThrowable() {
        return throwable;
    }

    @Nullable
    public T getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Resource<?> resource = (Resource<?>) o;

        if (status != resource.status) {
            return false;
        }
        if (throwable != null ? !throwable.equals(resource.throwable) : resource.throwable != null) {
            return false;
        }
        return data != null ? data.equals(resource.data) : resource.data == null;
    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + (throwable != null ? throwable.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Resource{" +
                "status=" + status +
                ", throwable='" + throwable + '\'' +
                ", data=" + data +
                '}';
    }
}

package by.svt.kirill.testtaskzimad.data.ws;

import by.svt.kirill.testtaskzimad.data.di.RetrofitService;
import by.svt.kirill.testtaskzimad.data.ws.api.MainListApiService;
import retrofit2.Retrofit;

public class ServicesApi {
    private Retrofit retrofit;

    public ServicesApi() {
        this.retrofit = RetrofitService
                .getInstance()
                .getRetrofit();
    }

    public MainListApiService getMainLIstApiService() {
        return retrofit.create(MainListApiService.class);
    }
}

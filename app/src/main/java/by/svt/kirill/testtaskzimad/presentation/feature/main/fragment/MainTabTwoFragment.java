package by.svt.kirill.testtaskzimad.presentation.feature.main.fragment;

import by.svt.kirill.testtaskzimad.R;

public class MainTabTwoFragment extends BaseTabFragment {
    public static final String TAG = MainTabTwoFragment.class.getSimpleName();
    private static final String PARAMS = "dog";

    public static MainTabTwoFragment getInstance() {
        return new MainTabTwoFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tab_fragment_view;
    }

    @Override
    protected String getRequestParams() {
        return PARAMS;
    }
}

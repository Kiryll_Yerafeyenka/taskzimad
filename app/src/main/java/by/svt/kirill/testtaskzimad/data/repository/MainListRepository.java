package by.svt.kirill.testtaskzimad.data.repository;

import java.util.List;

import by.svt.kirill.testtaskzimad.data.mapper.MainListMapper;
import by.svt.kirill.testtaskzimad.data.ws.ServicesApi;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.domain.dto.base.ServerResponse;
import by.svt.kirill.testtaskzimad.domain.repository.IMainListRepository;
import io.reactivex.Single;

public class MainListRepository implements IMainListRepository {
    private ServicesApi servicesApi = new ServicesApi();

    @Override
    public Single<ServerResponse<List<AnimalsParams>>> getAnimalsList(String params) {
        return servicesApi.getMainLIstApiService().getData(params).map(MainListMapper::map);
    }
}

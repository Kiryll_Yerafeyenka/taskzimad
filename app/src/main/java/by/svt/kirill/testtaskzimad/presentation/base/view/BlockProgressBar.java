package by.svt.kirill.testtaskzimad.presentation.base.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import by.svt.kirill.testtaskzimad.R;

public class BlockProgressBar extends FrameLayout implements IProgressView {

    public BlockProgressBar(@NonNull Context context) {
        super(context);
        init();
    }

    public BlockProgressBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BlockProgressBar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.block_progress_bar, this);
    }

    @Override
    public void showProgress() {
        this.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.setVisibility(GONE);
    }
}

package by.svt.kirill.testtaskzimad.domain.dto.base;

public class ServerResponse<T> {
    private String message;
    private T body;

    public ServerResponse(String message, T body) {
        this.message = message;
        this.body = body;
    }

    public String getMessage() {
        return message;
    }

    public T getBody() {
        return body;
    }
}

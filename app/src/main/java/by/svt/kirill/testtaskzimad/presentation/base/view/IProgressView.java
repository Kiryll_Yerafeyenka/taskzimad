package by.svt.kirill.testtaskzimad.presentation.base.view;

public interface IProgressView {
    void showProgress();

    void hideProgress();
}

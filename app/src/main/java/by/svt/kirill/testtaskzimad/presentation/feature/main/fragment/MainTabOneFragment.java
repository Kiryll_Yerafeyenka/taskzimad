package by.svt.kirill.testtaskzimad.presentation.feature.main.fragment;

import by.svt.kirill.testtaskzimad.R;

public class MainTabOneFragment extends BaseTabFragment {
    public static final String TAG = MainTabOneFragment.class.getSimpleName();
    private static final String PARAMS = "cat";

    public static MainTabOneFragment getInstance() {
        return new MainTabOneFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tab_fragment_view;
    }

    @Override
    protected String getRequestParams() {
        return PARAMS;
    }
}

package by.svt.kirill.testtaskzimad.domain.dto;

import java.io.Serializable;

public class AnimalsParams implements Serializable {
    private String url;
    private String title;
    private Integer number;

    public AnimalsParams(String url, String title, Integer number) {
        this.url = url;
        this.title = title;
        this.number = number;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getNumber() {
        return number.toString();
    }
}

package by.svt.kirill.testtaskzimad.domain.repository;

import java.util.List;

import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.domain.dto.base.ServerResponse;
import io.reactivex.Single;

public interface IMainListRepository {

    Single<ServerResponse<List<AnimalsParams>>> getAnimalsList(String params);
}

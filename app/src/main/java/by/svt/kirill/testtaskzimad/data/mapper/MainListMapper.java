package by.svt.kirill.testtaskzimad.data.mapper;

import com.annimon.stream.Stream;

import java.util.List;

import by.svt.kirill.testtaskzimad.data.ws.ServerResponseDto;
import by.svt.kirill.testtaskzimad.data.ws.dto.response.AnimalItemsResponse;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.domain.dto.base.ServerResponse;

public class MainListMapper {

    public static ServerResponse<List<AnimalsParams>> map(ServerResponseDto<List<AnimalItemsResponse>> model) {
        List<AnimalsParams> animalsParams =
                Stream.range(0, model.getData().size())
                        .map(it -> new AnimalsParams(model.getData().get(it).getUrl(),
                                model.getData().get(it).getTitle(), it + 1))
                        .toList();

        return new ServerResponse<>(model.getMessage(), animalsParams);
    }
}

package by.svt.kirill.testtaskzimad.presentation.feature.main.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.annimon.stream.Optional;
import com.google.android.material.tabs.TabLayout;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.presentation.base.view.IProgressView;
import by.svt.kirill.testtaskzimad.presentation.feature.main.fragment.MainTabOneFragment;
import by.svt.kirill.testtaskzimad.presentation.feature.main.fragment.MainTabTwoFragment;

public class MainActivity extends AppCompatActivity implements IProgressView {
    private static final String POSITION = "POSITION";

    private IProgressView progressView;
    private TabLayout tabLayout;
    private int basePosition = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        initView();
    }

    private void findViews() {
        progressView = findViewById(R.id.main_progress_bar);
        tabLayout = findViewById(R.id.main_bottom_navigation);
    }

    private void initView() {

        if (basePosition == -1) {
            transactionFragments(tabLayout.getSelectedTabPosition());
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                transactionFragments(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                clearContainer(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void transactionFragments(int position) {
        switch (position) {
            case 0:
                setSelectedTab(position);
                attachOneFragment();
                break;
            case 1:
                setSelectedTab(position);
                attachTwoFragment();
                break;
        }
    }

    private void clearContainer(int position) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                findFragmentByTag(MainTabOneFragment.TAG).ifPresent(t::detach);
                t.commit();
                break;
            case 1:
                findFragmentByTag(MainTabTwoFragment.TAG).ifPresent(t::detach);
                t.commit();
                break;
        }
    }

    private void setSelectedTab(int position) {
        Optional.ofNullable(tabLayout.getTabAt(position)).ifPresent(TabLayout.Tab::select);
    }

    private void attachOneFragment() {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        if (getSupportFragmentManager().findFragmentByTag(MainTabOneFragment.class.getSimpleName()) == null) {
            t.add(R.id.main_fragment_container, new MainTabOneFragment(), MainTabOneFragment.TAG);
            t.commit();
        } else {
            findFragmentByTag(MainTabTwoFragment.TAG).ifPresent(t::detach);
            findFragmentByTag(MainTabOneFragment.TAG).ifPresent(t::attach);
            t.commit();
        }
    }

    private void attachTwoFragment() {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        if (getSupportFragmentManager().findFragmentByTag(MainTabTwoFragment.class.getSimpleName()) == null) {
            t.add(R.id.main_fragment_container, new MainTabTwoFragment(), MainTabTwoFragment.TAG);
            t.commit();
        } else {
            findFragmentByTag(MainTabOneFragment.TAG).ifPresent(t::detach);
            findFragmentByTag(MainTabTwoFragment.TAG).ifPresent(t::attach);
            t.commit();
        }
    }

    private Optional<Fragment> findFragmentByTag(String tag) {
        return Optional.ofNullable(getSupportFragmentManager().findFragmentByTag(tag));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putInt(POSITION, tabLayout.getSelectedTabPosition());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        basePosition = savedInstanceState.getInt(POSITION, 0);
        transactionFragments(basePosition);
    }

    @Override
    public void showProgress() {
        progressView.showProgress();
    }

    @Override
    public void hideProgress() {
        progressView.hideProgress();
    }
}

package by.svt.kirill.testtaskzimad.data.ws.api;

import java.util.List;

import by.svt.kirill.testtaskzimad.data.ws.ServerResponseDto;
import by.svt.kirill.testtaskzimad.data.ws.dto.response.AnimalItemsResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MainListApiService {

    @GET("xim/api.php")
    Single<ServerResponseDto<List<AnimalItemsResponse>>> getData(@Query("query") String params);
}

package by.svt.kirill.testtaskzimad.presentation.feature.main.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.presentation.feature.main.activity.DetailItemActivity;
import by.svt.kirill.testtaskzimad.utils.PicassoUtils;

public class AnimalItemHolder extends RecyclerView.ViewHolder {
    private View view;
    private ImageView animalImage;
    private TextView animalNumber;
    private TextView animalTitle;
    private AnimalsParams actualModel;

    public AnimalItemHolder(@NonNull View itemView) {
        super(itemView);
        this.view = itemView;
        findViews();
    }

    private void findViews() {
        animalImage = view.findViewById(R.id.item_image_title);
        animalNumber = view.findViewById(R.id.item_number_text);
        animalTitle = view.findViewById(R.id.item_main_text);
    }

    public void populate(AnimalsParams model) {
        actualModel = model;
        animalNumber.setText(model.getNumber());
        animalTitle.setText(model.getTitle());
        PicassoUtils.loadImage(model.getUrl(), animalImage);
        setOnClickItemListener();
    }

    private void setOnClickItemListener() {
        view.setOnClickListener(v -> DetailItemActivity.startActivity(view.getContext(), actualModel));
    }
}

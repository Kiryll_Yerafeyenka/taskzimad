package by.svt.kirill.testtaskzimad.data.di;

import by.svt.kirill.testtaskzimad.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private static RetrofitService retrofitServiceInstance;
    private static final String BASE_URL = "http://kot3.com";
    private Retrofit mRetrofit;

    private RetrofitService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .addInterceptor(interceptor);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client.build())
                .build();
    }

    public static RetrofitService getInstance() {
        if (retrofitServiceInstance == null) {
            retrofitServiceInstance = new RetrofitService();
        }
        return retrofitServiceInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}

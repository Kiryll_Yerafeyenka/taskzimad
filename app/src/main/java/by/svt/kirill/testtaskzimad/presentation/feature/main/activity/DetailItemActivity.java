package by.svt.kirill.testtaskzimad.presentation.feature.main.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.annimon.stream.Optional;

import by.svt.kirill.testtaskzimad.R;
import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;
import by.svt.kirill.testtaskzimad.presentation.feature.main.fragment.DetailsFragment;

public class DetailItemActivity extends AppCompatActivity {
    public final static String EXTRA_ANIMAL_MODEL = "EXTRA_ANIMAL_MODEL";

    public static void startActivity(Context context, AnimalsParams animalModel) {
        Intent intent = new Intent(context, DetailItemActivity.class);
        intent.putExtra(EXTRA_ANIMAL_MODEL, animalModel);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empty_activity);
        addFragment();
    }

    private void addFragment() {
        if (!this.findFragment().isPresent()) {
            Fragment fragment = DetailsFragment.newInstance((AnimalsParams) getIntent().getSerializableExtra(EXTRA_ANIMAL_MODEL));
            this.getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.activity_empty_fragment_container, fragment)
                    .commit();
        }

    }

    private Optional<Fragment> findFragment() {
        return Optional.ofNullable(this.getSupportFragmentManager().findFragmentById(R.id.activity_empty_fragment_container));
    }

}

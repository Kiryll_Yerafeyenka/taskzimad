package by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview;

import by.svt.kirill.testtaskzimad.domain.dto.AnimalsParams;

public interface IDetailsItemFragmentView {
    void populate(AnimalsParams model);
}

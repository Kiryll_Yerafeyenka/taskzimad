package by.svt.kirill.testtaskzimad.domain.dto.base;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}

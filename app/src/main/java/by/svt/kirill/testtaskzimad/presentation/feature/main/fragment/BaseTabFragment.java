package by.svt.kirill.testtaskzimad.presentation.feature.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.annimon.stream.Optional;

import by.svt.kirill.testtaskzimad.domain.dto.base.Status;
import by.svt.kirill.testtaskzimad.presentation.base.view.IProgressView;
import by.svt.kirill.testtaskzimad.presentation.feature.main.fragmentview.ITabFragmentView;
import by.svt.kirill.testtaskzimad.presentation.feature.main.viewmodel.MainViewModel;

public abstract class BaseTabFragment extends Fragment {
    private ITabFragmentView fragmentView;
    private MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getLayoutId(), container, false);
        fragmentView = (ITabFragmentView) root;
        return root;
    }

    protected abstract int getLayoutId();

    protected abstract String getRequestParams();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        if (viewModel.getAnimalsParamsLiveData().getValue() == null || viewModel.getAnimalsParamsLiveData().getValue().getStatus() == Status.ERROR) {
            viewModel.getAnimalsList(getRequestParams());
        }
        subscribeAnimalsParams();
    }

    private void subscribeAnimalsParams() {
        viewModel.getAnimalsParamsLiveData().observe(this, serverResponseResource -> {
            switch (serverResponseResource.getStatus()) {
                case LOADING:
                    showProgress();
                    break;
                case SUCCESS:
                    hideProgress();
                    Optional.ofNullable(serverResponseResource.getData())
                            .ifPresent(it -> fragmentView.populate(it.getBody()));
                    break;
                case ERROR:
                    hideProgress();
                    Optional.ofNullable(serverResponseResource.getThrowable())
                            .ifPresent(it -> Toast.makeText(getContext(), it.getMessage(), Toast.LENGTH_SHORT).show());
                    break;
            }
        });
    }

    private void showProgress() {
        Optional.ofNullable(getActivity()).ifPresent(it -> ((IProgressView) it).showProgress());
    }

    private void hideProgress() {
        Optional.ofNullable(getActivity()).ifPresent(it -> ((IProgressView) it).hideProgress());
    }
}
